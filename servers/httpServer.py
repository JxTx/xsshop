#!/usr/bin/env python3
import http.server
import socketserver
import os


def cdPayloads(directory):
    web_dir = os.path.join(os.path.dirname(__file__), directory)
    os.chdir(web_dir)


def httpServer(port):
    Handler = http.server.SimpleHTTPRequestHandler
    httpd = socketserver.TCPServer(("", port), Handler)
    httpd.serve_forever()


payloadsDir = '../payloads'
serverPort = 8000

print("changing directory to {}".format(payloadsDir))
cdPayloads(payloadsDir)
print("http server on {}".format(serverPort))
httpServer(serverPort)
