#!/usr/bin/env python3
import http.server
import socketserver
import ssl
import os


def cdPayloads(directory):
    web_dir = os.path.join(os.path.dirname(__file__), directory)
    os.chdir(web_dir)


def httpServer(port, cert, key):
    Handler = http.server.SimpleHTTPRequestHandler
    httpd = socketserver.TCPServer(("", port), Handler)
    httpd.socket = ssl.wrap_socket(httpd.socket,
                                   server_side=True,
                                   certfile=cert,
                                   keyfile=key)
    httpd.serve_forever()


# openssl req -new -x509 -keyout l33t_key.pem -out l33t.pem -days 1 -nodes
certFile = '../servers/l33t.pem'
keyFile = '../servers/l33t_key.pem'

payloadsDir = '../payloads'
serverPort = 8443

print("changing directory to {}".format(payloadsDir))
cdPayloads(payloadsDir)
print("http server on {}".format(serverPort))
httpServer(serverPort, certFile, keyFile)
